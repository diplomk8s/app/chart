Для развертывания чарта в k8s кластере

1) склонируйте репозиторий

git clone git@gitlab.com:diplomk8s/app/chart.git

или 

git clone https://gitlab.com/diplomk8s/app/chart.git


2) добавьте kubeconfig в base64 в variables проекта

3) зарегистрируйте раннер по инструкции https://docs.gitlab.com/runner/install/

4) запустите пайплайн


Если нужно поменять образ, то необходимо поменять variables в файл .gitlab-ci.yml
